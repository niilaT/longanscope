/*!
    \file  main.c
    \brief USB main routine for MSC device(Udisk)

    \version 2019-6-5, V1.0.0, firmware for GD32VF103
*/

/*
    Copyright (c) 2019, GigaDevice Semiconductor Inc.
    Copyright (c) 2022, ntt

    Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this 
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice, 
       this list of conditions and the following disclaimer in the documentation 
       and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors 
       may be used to endorse or promote products derived from this software without 
       specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
OF SUCH DAMAGE.
*/



#include "drv_usb_hw.h"
#include "usbd_msc_core.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "sram_msd.h"
#include "scopeconfig.h"


usb_core_driver USB_OTG_dev = {
    .dev = {
        .desc = {
            .dev_desc    = (uint8_t *)&msc_dev_desc,
            .config_desc = (uint8_t *)&msc_config_desc,
            .strings     = usbd_msc_strings
        }
    }
};



volatile bool w_o = false;
volatile uint8_t IObits = 0;
volatile uint8_t bitnumber = 0;




int main(void)
{
    eclic_global_interrupt_enable();

    eclic_priority_group_set(ECLIC_PRIGROUP_LEVEL3_PRIO1); //levels 0-7, priorities 0-1

    //usb init
    usb_rcu_config();
    usb_timer_init();
    usb_intr_config();
    usbd_init(&USB_OTG_dev, USB_CORE_ENUM_FS, &msc_class);

    //led init
    rcu_periph_clock_enable(RCU_GPIOC);
    gpio_init(GPIOC, GPIO_MODE_OUT_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_13); //red led
    gpio_bit_set(GPIOC, GPIO_PIN_13); //off
    

    rcu_periph_clock_enable(RCU_DMA0);
    rcu_periph_clock_enable(RCU_GPIOA);

    
    /*DMA init for SRAM read*/
    dma_deinit(DMA0, DMA_CH3);
    dma_parameter_struct dmaS_data_parameter;
    dmaS_data_parameter.memory_addr  = (uint32_t)(NULL);
    dmaS_data_parameter.memory_inc   = DMA_MEMORY_INCREASE_ENABLE;
    dmaS_data_parameter.memory_width = DMA_MEMORY_WIDTH_8BIT;
    dmaS_data_parameter.periph_addr  = (uint32_t)(NULL);
    dmaS_data_parameter.periph_inc   = DMA_PERIPH_INCREASE_ENABLE;
    dmaS_data_parameter.periph_width = DMA_MEMORY_WIDTH_8BIT;
    dmaS_data_parameter.direction    = DMA_MEMORY_TO_PERIPHERAL;
    dmaS_data_parameter.number       = 0;
    dmaS_data_parameter.priority     = DMA_PRIORITY_ULTRA_HIGH;
    dma_init(DMA0, DMA_CH3, &dmaS_data_parameter);
    dma_circulation_disable(DMA0, DMA_CH3);
    dma_memory_to_memory_enable(DMA0, DMA_CH3);
    /*DMA init for SRAM read end*/





    #if defined(ONE_ADC) || defined(TWO_CHANNEL_SEQ) || defined(TWO_ADC_PAR)
    
    //timer init for ADC
    rcu_periph_clock_enable(RCU_TIMER2);
    timer_deinit(TIMER2);

    //system clock is 96MHz, APB2=/1 (timer0, ADC etc), APB1=/2 =48MHz for APB1 peripherals but x2(timer2 etc)
    //->TIMER2 clock 96(2x48)MHz
    timer_parameter_struct tp;
    tp.clockdivision = TIMER_CKDIV_DIV1; 
    tp.prescaler = 0; //timer clock for counter is divided by prescaler+1 (1-65536) -> 96MHz
    #if defined(ONE_ADC)
    tp.period = 119; //16-bit(limit max 65536) counter period: value+1 (counts from 0) -> 800kHz
    #elif defined(TWO_CHANNEL_SEQ)
    tp.period = 239; //-> 400kHz
    #elif defined(TWO_ADC_PAR)
    tp.period = 479; //-> 200kHz
    #endif
    tp.repetitioncounter = 0; // generate update event every time counter overflows
    tp.alignedmode = TIMER_COUNTER_EDGE;
    tp.counterdirection = TIMER_COUNTER_UP;
    timer_init(TIMER2, &tp); //apply the parameters

    //timer_single_pulse_mode_config(TIMER3,TIMER_SP_MODE_REPETITIVE); //default
    timer_master_output_trigger_source_select(TIMER2,TIMER_TRI_OUT_SRC_UPDATE); //generate TRGO on Update



    //ADC init (ADC0, pin PA0)
    //pin config
    gpio_init(GPIOA, GPIO_MODE_AIN, GPIO_OSPEED_50MHZ, GPIO_PIN_0); //PA0
    #if defined(TWO_CHANNEL_SEQ) || defined(TWO_ADC_PAR)
    gpio_init(GPIOA, GPIO_MODE_AIN, GPIO_OSPEED_50MHZ, GPIO_PIN_3); //PA3
    #endif
    //ADC init
    rcu_adc_clock_config(RCU_CKADC_CKAPB2_DIV8); //96MHz/8=12MHz (ADC max is 14MHz)

    rcu_periph_clock_enable(RCU_ADC0);
    adc_deinit(ADC0);


    #ifdef TWO_ADC_PAR
    rcu_periph_clock_enable(RCU_ADC1);
    adc_deinit(ADC1);
    //both ADCs capture at the same time; ADC1 result is moved to upper 16 bits of RDATA(ADC0), ADC0 to lower 16 bits
    adc_mode_config(ADC_DAUL_REGULAL_PARALLEL); 
    
    adc_data_alignment_config(ADC1,ADC_DATAALIGN_RIGHT);
    adc_regular_channel_config(ADC1,0,ADC_CHANNEL_3,ADC_SAMPLETIME_1POINT5);
    adc_resolution_config(ADC1,ADC_RESOLUTION_8B);

    adc_external_trigger_source_config(ADC1,ADC_REGULAR_CHANNEL,ADC0_1_EXTTRIG_REGULAR_T2_TRGO);
    adc_external_trigger_config(ADC1,ADC_REGULAR_CHANNEL,ENABLE);
    #elif defined(TWO_CHANNEL_SEQ)
    //convert multiple channels in one ADC sequentially, could have one interrupt after all have been converted
    adc_special_function_config(ADC0,ADC_SCAN_MODE,ENABLE);
    //using regular channels in scan mode requires using DMA but inserted channels (ADC0-3) have each their own data registers and could be read from the conversion end interrupt
    adc_channel_length_config(ADC0,ADC_REGULAR_CHANNEL,2);
    adc_regular_channel_config(ADC0,1,ADC_CHANNEL_3,ADC_SAMPLETIME_1POINT5);
    //TODO possibly because the channel reads are really close to each other and the ADC cap doesn't have enough time to discharge after the first channel read, there seems to be some "crosstalk" from the first channel reading to the second channel. Longer delay inbetween might fix this.
    #else
    //ADCs work independently
    adc_mode_config(ADC_MODE_FREE);
    #endif

    adc_data_alignment_config(ADC0,ADC_DATAALIGN_RIGHT);
    adc_regular_channel_config(ADC0,0,ADC_CHANNEL_0,ADC_SAMPLETIME_1POINT5);
    adc_resolution_config(ADC0,ADC_RESOLUTION_8B);

    adc_external_trigger_source_config(ADC0,ADC_REGULAR_CHANNEL,ADC0_1_EXTTRIG_REGULAR_T2_TRGO);
    adc_external_trigger_config(ADC0,ADC_REGULAR_CHANNEL,ENABLE);

    #ifdef TWO_ADC_PAR
    adc_interrupt_flag_clear(ADC1, ADC_INT_FLAG_EOC);
    adc_interrupt_flag_clear(ADC1, ADC_INT_FLAG_EOIC);
    adc_enable(ADC1);

    adc_calibration_enable(ADC1);
    #endif
    adc_interrupt_flag_clear(ADC0, ADC_INT_FLAG_EOC);
    adc_interrupt_flag_clear(ADC0, ADC_INT_FLAG_EOIC);
    adc_enable(ADC0);
    //TODO wait 14 ADCclocks after enabling for stability (1,2uS @ 12MHz)
    adc_calibration_enable(ADC0); //calibrate, waits automatically for completion

    //don't enable ADC interrupt as we are using DMA instead
    //eclic_irq_enable(ADC0_1_IRQn, 4, 0);
    //adc_interrupt_enable(ADC0, ADC_INT_EOC);


    dma_deinit(DMA0, DMA_CH0);
    dma_parameter_struct dma_data_parameter;
    dma_data_parameter.periph_addr  = (uint32_t)(&ADC_RDATA(ADC0));
    dma_data_parameter.periph_inc   = DMA_PERIPH_INCREASE_DISABLE;
    dma_data_parameter.periph_width = DMA_PERIPHERAL_WIDTH_32BIT;
    dma_data_parameter.memory_addr  = (uint32_t)(&SRAM[0]);
    dma_data_parameter.memory_inc   = DMA_MEMORY_INCREASE_ENABLE;
    #ifdef TWO_ADC_PAR
    dma_data_parameter.memory_width = DMA_MEMORY_WIDTH_32BIT; //DMA doesn't support skipping addresses so in 2-ADC mode we get 2 8-bit values in 2 16-bit halves of the register - half the bandwidth goes to waste... Could also use simultaneous non-parallel mode and two DMAs to separate buffers, at least in burst mode.
    dma_data_parameter.number       = (BUFFER_SIZE*NUM_OF_BUFFERS)/4;
    #else
    dma_data_parameter.memory_width = DMA_MEMORY_WIDTH_8BIT;
    dma_data_parameter.number       = BUFFER_SIZE*NUM_OF_BUFFERS; //number of transfers
    #endif
    dma_data_parameter.direction    = DMA_PERIPHERAL_TO_MEMORY;
    dma_data_parameter.priority     = DMA_PRIORITY_ULTRA_HIGH;
    dma_init(DMA0, DMA_CH0, &dma_data_parameter);
    
    dma_memory_to_memory_disable(DMA0, DMA_CH0);
    #ifndef BURSTMODE
    dma_circulation_enable(DMA0, DMA_CH0);
    #endif

    dma_interrupt_flag_clear(DMA0, DMA_CH0, DMA_INT_FLAG_FTF);
    dma_interrupt_enable(DMA0, DMA_CH0, DMA_INT_FLAG_FTF);
    eclic_irq_enable(DMA0_Channel0_IRQn, 4, 0);
  
    /* enable DMA channel */
    dma_channel_enable(DMA0, DMA_CH0);

    adc_dma_mode_enable(ADC0);

    #ifndef BURSTMODE
    timer_enable(TIMER2);
    #endif

    #endif //ONE_ADC || TWO_CHANNEL_SEQ || TWO_ADC_PAR




    #ifdef PIN_1BIT

    gpio_init(GPIOA, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, GPIO_PIN_0); //PA0

    rcu_periph_clock_enable(RCU_TIMER3);
    timer_deinit(TIMER3);

    timer_parameter_struct tp3;
    tp3.clockdivision = TIMER_CKDIV_DIV1; 
    tp3.prescaler = 0; //timer clock divided by prescaler+1 -> 96MHz
    tp3.period = 120;
    tp3.repetitioncounter = 0; // generate update event every time counter overflows
    tp3.alignedmode = TIMER_COUNTER_EDGE;
    tp3.counterdirection = TIMER_COUNTER_UP;
    timer_init(TIMER3, &tp3); //apply the parameters
    
    //timer_single_pulse_mode_config(TIMER3,TIMER_SP_MODE_REPETITIVE); //default
    timer_update_event_enable(TIMER3);
    timer_interrupt_flag_clear(TIMER3, TIMER_INT_FLAG_UP);
    #ifndef BURSTMODE
    timer_interrupt_enable(TIMER3,TIMER_INT_UP);
    timer_enable(TIMER3);
    eclic_irq_enable(TIMER3_IRQn, 4, 0);
    #endif

    #endif



    while (1) {}
}



#if defined(PIN_1BIT) && !defined(BURSTMODE)
//TODO not surprisingly, the core can't run interrupts fast enough to get any reasonable pin sampling speed; instead could hack SPI to capture the bits
void TIMER3_IRQHandler(void)
{
    IObits = (IObits<<1) | (GPIO_ISTAT(GPIOA) & (GPIO_PIN_0)) /*gpio_input_bit_get(GPIOA, GPIO_PIN_0)*/;

    if(bitnumber==7)
    {
        SRAM[writepos_software]=IObits;
        if((writepos_software+1)==BUFFER_SIZE*NUM_OF_BUFFERS)
        {
            if(w_o) //double-cross buffer end overflow
            {
                overflow=true;
                gpio_bit_reset(GPIOC, GPIO_PIN_13); //led on
            }

            //set flag when crossed end of the buffer
            w_o = true;
        }
        writepos_software = (writepos_software+1) % (BUFFER_SIZE*NUM_OF_BUFFERS);
    }

    bitnumber++;
    bitnumber = bitnumber%8;
    

    TIMER_INTF(TIMER3) = (~(uint32_t)TIMER_FLAG_UP);
}
#endif //PIN_1BIT !BURSTMODE




#if defined(ONE_ADC) || defined(TWO_CHANNEL_SEQ) || defined(TWO_ADC_PAR)

void DMA0_Channel0_IRQHandler(void)
{
    #ifndef BURSTMODE
    if(w_o) //double-cross buffer end overflow
    {
        overflow=true;
        gpio_bit_reset(GPIOC, GPIO_PIN_13); //led on
    } 
    #else
    timer_disable(TIMER2); //burst over, disable ADC trigger timer
    #endif
    //set flag when crossed end of the buffer
    w_o = true;

    //clear interrupt
    DMA_INTC(DMA0) |= DMA_FLAG_ADD(DMA_INT_FLAG_FTF, DMA_CH0);

}
#endif //ONE_ADC || TWO_CHANNEL_SEQ || TWO_ADC_PAR
