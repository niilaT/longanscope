/*!
    \file    sram_msd.c
    \brief   internal flash functions
    \version 2021-10-30, V1.0.0, firmware for GD32W51x
*/

/*
    Copyright (c) 2021, GigaDevice Semiconductor Inc.
    Copyright (c) 2022, ntt

    Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright notice, this 
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice, 
       this list of conditions and the following disclaimer in the documentation 
       and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors 
       may be used to endorse or promote products derived from this software without 
       specific prior written permission.
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
OF SUCH DAMAGE.
*/

#include "usb_conf.h"
#include "sram_msd.h"

#define min(a,b) ({ __typeof__ (a) _a = (a); __typeof__ (b) _b = (b); _a < _b ? _a : _b; })


#ifdef WRITEPOS_SOFTWARE
volatile uint16_t writepos_software = 0;
#else 
#define WRITEPOS_DMA0CH0
#endif


#ifdef BURSTMODE
volatile bool burststarted = false;
#endif

unsigned volatile char SRAM[BUFFER_SIZE*NUM_OF_BUFFERS];


volatile bool overflow = true;

volatile uint8_t READ_buffer_number = 0;
volatile uint32_t blocksInBuffer = 0;





/*!
    \brief      read data from multiple blocks of SRAM
    \param[in]  pBuf: pointer to user buffer
    \param[in]  ReadAddr: address of 1st block to be read
    \param[in]  BlkSize: size of block
    \param[in]  BlkNum: number of blocks
    \param[out] none
    \retval     status
*/
uint32_t SRAM_ReadMultiBlocks (uint8_t *pBuf, uint32_t ReadAddr, uint16_t BlkSize, uint32_t BlkNum)
{
    uint8_t* realaddr =  &SRAM[0] + (BUFFER_SIZE * READ_buffer_number) + (ReadAddr % BUFFER_SIZE);


    #ifdef BURSTMODE
    if (ReadAddr==0) //if read is to the beginning of the memory, restart burst capture
    {
        //these don't really belong here but since the whole tool is quite hacky anyways...
        w_o = false;
        #ifdef ONE_ADC
        realaddr = &SRAM[0];
        timer_disable(TIMER2);
        dma_channel_disable(DMA0, DMA_CH0);
        dma_memory_address_config(DMA0, DMA_CH0, (uint32_t)realaddr);
        dma_transfer_number_config(DMA0, DMA_CH0, BUFFER_SIZE*NUM_OF_BUFFERS);
        timer_autoreload_value_config(TIMER2, 79); //10 ADC clocks per 8-bit sample, max at 12MHz: 1,2MS
        timer_event_software_generate(TIMER2, TIMER_EVENT_SRC_UPG); //load autoreload value
        timer_flag_clear(TIMER2,TIMER_FLAG_UP); //clear to not cause one dummy DMA transfer
        dma_channel_enable(DMA0, DMA_CH0);
        timer_enable(TIMER2);
        #elif defined(TWO_CHANNEL_SEQ)
        realaddr = &SRAM[0];
        timer_disable(TIMER2);
        dma_channel_disable(DMA0, DMA_CH0);
        dma_memory_address_config(DMA0, DMA_CH0, (uint32_t)realaddr);
        dma_transfer_number_config(DMA0, DMA_CH0, BUFFER_SIZE*NUM_OF_BUFFERS);
        dma_channel_enable(DMA0, DMA_CH0);
        timer_autoreload_value_config(TIMER2, 159); //600kS / channel
        timer_event_software_generate(TIMER2, TIMER_EVENT_SRC_UPG);
        timer_flag_clear(TIMER2,TIMER_FLAG_UP);
        timer_enable(TIMER2);
        #elif defined(TWO_ADC_PAR)
        realaddr = &SRAM[0];
        timer_disable(TIMER2);
        dma_channel_disable(DMA0, DMA_CH0);
        dma_memory_address_config(DMA0, DMA_CH0, (uint32_t)realaddr);
        dma_transfer_number_config(DMA0, DMA_CH0, (BUFFER_SIZE*NUM_OF_BUFFERS)/4);
        dma_channel_enable(DMA0, DMA_CH0);
        timer_autoreload_value_config(TIMER2, 79); //1,2MS / channel
        timer_event_software_generate(TIMER2, TIMER_EVENT_SRC_UPG);
        timer_flag_clear(TIMER2,TIMER_FLAG_UP);
        timer_enable(TIMER2);
        #elif defined(PIN_1BIT)
        timer_prescaler_config(TIMER3,11,TIMER_PSC_RELOAD_UPDATE); //set prescaler to 11+1 -> 8MHz
        timer_autoreload_value_config(TIMER3, 3); //in up-counting mode sets the value to count to from 0 -> 8/(3+1): 2MHz total counter time BUT use alternative cycles 0 and 2 -> 4MS
        timer_event_software_generate(TIMER3, TIMER_EVENT_SRC_UPG); //in up-counting mode resets counter to 0 and setups the autoreload and prescaler values
        //disable interrupts and quickly capture while USB is waiting for the response
        eclic_global_interrupt_disable();
        timer_enable(TIMER3);
        uint8_t IObits = 0;
        //balance instructions at the end of the loop
        while ( TIMER_CNT(TIMER3) != 0 ){};
        unsigned int pinvalue = (GPIO_ISTAT(GPIOA) & (GPIO_PIN_0));
        IObits = (IObits<<1) | pinvalue;            
        for (uint32_t i=0; i<BUFFER_SIZE*NUM_OF_BUFFERS; ++i)
        {
            //not necessarily very accurate at highest speeds, might introduce jitter... or the counter reading instruction never happens at same moment as the counter is at certain value if reading the counter register takes longer than one counter increment; too slow value might not change between reads so there wouldn't be any wait -> use alternative 0,2,0,2...
            while ( TIMER_CNT(TIMER3) != 2 ){}; // assembly code seems to result in 2 instructions for the while checks and 5 instructions inbetween to store the pin value
            pinvalue = (GPIO_ISTAT(GPIOA) & (GPIO_PIN_0));
            IObits = (IObits<<1) | pinvalue;
            while ( TIMER_CNT(TIMER3) != 0 ){};
            pinvalue = (GPIO_ISTAT(GPIOA) & (GPIO_PIN_0));
            IObits = (IObits<<1) | pinvalue;
            while ( TIMER_CNT(TIMER3) != 2 ){};
            pinvalue = (GPIO_ISTAT(GPIOA) & (GPIO_PIN_0));
            IObits = (IObits<<1) | pinvalue;
            while ( TIMER_CNT(TIMER3) != 0 ){};
            pinvalue = (GPIO_ISTAT(GPIOA) & (GPIO_PIN_0));
            IObits = (IObits<<1) | pinvalue;
            while ( TIMER_CNT(TIMER3) != 2 ){};
            pinvalue = (GPIO_ISTAT(GPIOA) & (GPIO_PIN_0));
            IObits = (IObits<<1) | pinvalue;
            while ( TIMER_CNT(TIMER3) != 0 ){};
            pinvalue = (GPIO_ISTAT(GPIOA) & (GPIO_PIN_0));
            IObits = (IObits<<1) | pinvalue;
            while ( TIMER_CNT(TIMER3) != 2 ){};
            pinvalue = (GPIO_ISTAT(GPIOA) & (GPIO_PIN_0));
            SRAM[i] = (IObits<<1) | pinvalue; //couple extra assembly instructions from store
            while ( TIMER_CNT(TIMER3) != 0 ){}; //not used in the last loop iteration
            pinvalue = (GPIO_ISTAT(GPIOA) & (GPIO_PIN_0));
            IObits = (IObits<<1) | pinvalue;
            //extra instructions from branch back
        }
        timer_disable(TIMER3);
        eclic_global_interrupt_enable();
        w_o = true;
        writepos_software = BUFFER_SIZE*NUM_OF_BUFFERS; //fake write position to after end of the buffer
        #endif

        //set the read position to beginning
        READ_buffer_number = 0;
        overflow = false;
    }
    #else
    if (ReadAddr==0)
    {
        gpio_bit_set(GPIOC, GPIO_PIN_13); //led off
        overflow = true; //use overflow mechanism to reset read position
        // TODO could always restart capture from pos 0 for more accurate capture start control
    }
    #endif
    
    

    while (BlkNum--)
    {

        #ifndef BURSTMODE
        //check for overflow, means that the previously returned block was corrupt
        if (overflow)
        {
            //reset realaddr to beginning of the read buffer where write address is currently -> always returns up to one buffer of old data
            #ifdef WRITEPOS_DMA0CH0
                #ifdef TWO_ADC_PAR
                uint16_t currentwritepos = (BUFFER_SIZE*NUM_OF_BUFFERS)-DMA_CHCNT(DMA0, DMA_CH0)*4; // 0 - m
                #else
                uint16_t currentwritepos = (BUFFER_SIZE*NUM_OF_BUFFERS)-DMA_CHCNT(DMA0, DMA_CH0); // 0 - m
                #endif
                if (currentwritepos == (BUFFER_SIZE*NUM_OF_BUFFERS)) currentwritepos=0; //-> 0 - m-1
            #elif defined(WRITEPOS_SOFTWARE)
                uint16_t currentwritepos = writepos_software;
            #endif
            READ_buffer_number = currentwritepos / BUFFER_SIZE;
            realaddr =  &SRAM[0] + (BUFFER_SIZE * READ_buffer_number) + (ReadAddr % BUFFER_SIZE);
            //if write was at the very beginning of the block, wait for it to advance 1 address
            if (&SRAM[0]+currentwritepos == realaddr)
            {
                #ifdef WRITEPOS_DMA0CH0
                    #ifdef TWO_ADC_PAR
                    while((BUFFER_SIZE*NUM_OF_BUFFERS)-DMA_CHCNT(DMA0, DMA_CH0)*4 == currentwritepos){};
                    #else
                    while((BUFFER_SIZE*NUM_OF_BUFFERS)-DMA_CHCNT(DMA0, DMA_CH0) == currentwritepos){};
                    #endif
                #elif defined(WRITEPOS_SOFTWARE)
                    while(writepos_software == currentwritepos){};
                #endif
            }
            if(READ_buffer_number != NUM_OF_BUFFERS-1) w_o = false;

            overflow = false;
        }
        #endif //!BURSTMODE


        

        for (uint32_t i = 0; i < BlkSize; ++i)
        {

            //readpos: this position will be read next
            //writepos: this position will be written next
            //readpos can equal to writepos but must not be read before writepos has advanced

            uint16_t readpos = realaddr-&SRAM[0]; //0 - m-1
            uint16_t writepos;
            do {
            #ifdef WRITEPOS_DMA0CH0
                #ifdef TWO_ADC_PAR
                writepos = (BUFFER_SIZE*NUM_OF_BUFFERS)-DMA_CHCNT(DMA0, DMA_CH0)*4; // 0 - m
                #else
                writepos = (BUFFER_SIZE*NUM_OF_BUFFERS)-DMA_CHCNT(DMA0, DMA_CH0); // 0 - m
                #endif
                #ifndef BURSTMODE
                if (writepos == (BUFFER_SIZE*NUM_OF_BUFFERS)) writepos=0; //-> 0 - m-1
                #endif
            #elif defined(WRITEPOS_SOFTWARE)
                writepos = writepos_software;
            #endif
            } while (readpos==writepos); //wait for writepos to advance if we can't read yet

            #ifndef BURSTMODE
            //check for overflow TODO expand detection so that DMA into same buffer during read is a (potential) overflow
            if(w_o && writepos>readpos) //writepos went around, past readpos TODO technically, writepos could go just past readpos momentarily and not be detected
            {
                overflow = true;
                gpio_bit_reset(GPIOC, GPIO_PIN_13); //led on
            }
            #endif //!BURSTMODE


            //if writepos is good amount ahead let's use DMA for those items (if DMA is ready)
            bool DMAready = DMA_CHCNT(DMA0, DMA_CH3)==0;
            bool enough = ((writepos - readpos) > 10) || w_o ;
            bool notlast = (BUFFER_SIZE*NUM_OF_BUFFERS)-readpos > 10;
            if ( enough && notlast && DMAready )
            {
                DMA_CHCTL(DMA0, DMA_CH3) &= ~DMA_CHXCTL_CHEN;//dma_channel_disable(DMA0, DMA_CH3);
                DMA_CHMADDR(DMA0, DMA_CH3) = (uint32_t)realaddr;//dma_memory_address_config(DMA0, DMA_CH3, (uint32_t)realaddr);
                DMA_CHPADDR(DMA0, DMA_CH3) = (uint32_t)pBuf;//dma_periph_address_config(DMA0, DMA_CH3, (uint32_t)pBuf);
                //min of (writepos-readpos OR max-readpos) OR BlkSize (crossing over circular buffer border is handled manually)
                uint16_t n = min( w_o ? ((BUFFER_SIZE*NUM_OF_BUFFERS)-readpos) : (writepos-readpos), BlkSize );
                DMA_CHCNT(DMA0, DMA_CH3) = (n & DMA_CHANNEL_CNT_MASK);//dma_transfer_number_config(DMA0, DMA_CH3, n);
                DMA_CHCTL(DMA0, DMA_CH3) |= DMA_CHXCTL_CHEN;//dma_channel_enable(DMA0, DMA_CH3);
                
                readpos += n;
                realaddr += n;
                pBuf += n;
                i += n-1; // i added 1 at end of the loop automatically
            }
            else //otherwise copy item manually
            {
                *pBuf = *(realaddr);
                readpos++;
                realaddr++;
                pBuf++;
            }
            //pBuf items are later read sequentially, so letting DMA copy on background is safe as long as DMA has written first item before it's read (and doesn't stall)


            if (readpos > (BUFFER_SIZE*NUM_OF_BUFFERS-1)) //we read the last position; reset to the beginning
            {
                realaddr = &SRAM[0];
                w_o = false;
            }

        }


        blocksInBuffer++;
        if (blocksInBuffer==(BUFFER_SIZE/ISRAM_BLOCK_SIZE))
        {
            READ_buffer_number = (READ_buffer_number+1) % NUM_OF_BUFFERS;
            blocksInBuffer=0;
        }
    }

    return 0;
}

/*!
    \brief      write data to multiple blocks of SRAM
    \param[in]  pBuf: pointer to user buffer
    \param[in]  WriteAddr: address of 1st block to be written
    \param[in]  BlkSize: size of block
    \param[in]  BlkNum: number of blocks
    \param[out] none
    \retval     status
*/
uint32_t SRAM_WriteMultiBlocks(uint8_t *pBuf, uint32_t WriteAddr, uint16_t BlkSize, uint32_t BlkNum)
{
    return 0U;
}
