longanSCOPE

Quick, hacky and non-portable analog-to-digital reader for Longan Nano, made using technologies that were easily available for the platform.
Low-rate "poor man's scope" if you haven't got one but have got a Longan Nano. Really WIP but I don't think I'll ever bother to go back to improve it.

The interface to PC is a simple implementation on top of a MSC storage device example by Gigadevice (because it allows USB bulk mode transfers).
The device will appear on PC as a raw storage device from which data can be read eg. on Linux with dd if=/dev/sdd of=./out.raw

Longan Nano has only Full Speed USB which doesn't even seem to support DMA.
That limits maximum transfer speed to about 1MB/s using the Gigadevice USB library, without other overhead.

Operating mode and virtual storage size can be selected in scopeconfig.h; TIMER2 in main.c generates the sampling rate which is already set to ~ maximum usable.
If you're using the Longan Nano Lite with only 20K SRAM you'll have to reduce NUM_OF_BUFFERS to half.
With smaller buffers or longer capture size set TIMER2 use slower sampling rate, otherwise the circular buffer will overflow.
Overflow can still occur with the defaults (red led lights up during capture) if eg. PC has a delay in reading or you look at it the wrong way - lower timing to eg. 500kS if you're having problems with reliability.

Modes and their default sampling rates:
ONE_ADC: 800kS, 8-bit ADC
TWO_CHANNEL_SEQ: 400kS/channel, two 8-bit ADC channels converted sequentially (a conversion-length delay inbetween) from different pins
TWO_ADC_PAR: 200kS/channel, two 8-bit ADC conversions at exact same time in parallel from different pins; output has 8 bits of data for every 16 bits.
PIN_1BIT: 4MS, 1-bit data (reads logic level of a pin); only BURSTMODE supported, continuous capture unimplemented

BURSTMODE allows you to capture at faster rate but only one buffer of data (~25kB by default).
Sampling rates in BURSTMODE for the ADC modes above are 1.2MS, 600kS/channel and 1.2MS/channel, respectively.
In non-BURSTMODE the default virtual storage/"continuous" capture size is 10MB.

Uses pins PA0 and PA3. Note that in ADC modes average input resistance during sampling may be low >20kOhms so use additional series resistor if that would affect your circuit.
