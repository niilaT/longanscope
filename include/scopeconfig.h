/*
    Copyright (c) 2022, ntt

    Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright notice, this 
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice, 
       this list of conditions and the following disclaimer in the documentation 
       and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors 
       may be used to endorse or promote products derived from this software without 
       specific prior written permission.
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
OF SUCH DAMAGE.
*/

#ifndef __SCOPECONFIG
#define __SCOPECONFIG


/*MODE*/
#define ONE_ADC
//#define TWO_CHANNEL_SEQ
//#define TWO_ADC_PAR
//#define PIN_1BIT

/*continuous capture or a single fast burst that fits into the SRAM*/
//#define BURSTMODE



/*buffer length <64kB (DMA limit) and <32kB (SRAM amount)*/
#define BUFFER_SIZE (64*8*4)
#define NUM_OF_BUFFERS (12)


/*number of blocks of the virtual storage in non-burst mode*/
#ifndef BURSTMODE
#define ISRAM_BLOCK_NUM          (2U*10000U)      /*10MB (total length of a capture for PC)*/
#endif






/*       --------------       do not edit        ----------------         */

/*block size of the virtual storage for PC*/
#define ISRAM_BLOCK_SIZE         512U 
#if defined(ONE_ADC) + defined(TWO_CHANNEL_SEQ) + defined(TWO_ADC_PAR) + defined(PIN_1BIT) != 1
    #error "define only one mode"
#endif
#ifdef PIN_1BIT
    #ifndef BURSTMODE
        #pragma message "only BURSTMODE implemented for PIN_1BIT mode"
        #define BURSTMODE
    #endif
#endif
#ifdef BURSTMODE
    #define ISRAM_BLOCK_NUM          (BUFFER_SIZE/ISRAM_BLOCK_SIZE)*NUM_OF_BUFFERS
#endif
#if BUFFER_SIZE % ISRAM_BLOCK_SIZE != 0
    #error "BUFFER_SIZE needs to be 1x-nx multiple of ISRAM_BLOCK_SIZE"
#endif
#ifdef ONE_ADC
    #define WRITEPOS_DMA0CH0
#endif
#ifdef TWO_CHANNEL_SEQ
    #define WRITEPOS_DMA0CH0
#endif
#ifdef TWO_ADC_PAR
    #define WRITEPOS_DMA0CH0
#endif
#ifdef PIN_1BIT
    #define WRITEPOS_SOFTWARE
#endif


#endif /*__SCOPECONFIG*/